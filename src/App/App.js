import mapboxgl from 'mapbox-gl'
import "../styles/style.scss"

import config from '../../app.config.json'

import { FormEvent } from './FormEventComponent/FormEvent'
import { MapControl } from './MapControlComponent/MapControl'
class App {
    map;
    domParser;
    domEventContainer;

    #_formEvent;
    get formEvent() { return this.#_formEvent; }

    constructor() {
        mapboxgl.accessToken = config.apis.mapbox_gl.api_key;

        this.domParser = new DOMParser();
        this.domEventContainer = document.getElementById('form-container');
        
    }
    start(){
        console.log('Aplication en marche');
        const centerMap = new mapboxgl.LngLat( 2.9793,43.0289 );
        
        this.map = new mapboxgl.Map({
            container: 'map',
            style: config.apis.mapbox_gl.map_style,
            center: centerMap,
            zoom: 16.5
        });
        

        this.#_formEvent = new FormEvent();
        this.renderEvent();

        navigator.geolocation.getCurrentPosition( this.onPositionFound.bind( this ) );
    }

    renderEvent(){        
        this.formEvent.render( this.domEventContainer );

    }

    onPositionFound( geoData ) {
        const currentLngLat = {
            lat: geoData.coords.latitude,
            lng: geoData.coords.longitude
        };

        // Creation de la popup du marker
        const popup = new mapboxgl.Popup();

        this.currentPosMarker = new mapboxgl.Marker();
        this.currentPosMarker
            .setLngLat( currentLngLat )
            .setPopup( popup )
            .addTo( this.map );

    }
}

const app = new App();
export default app;