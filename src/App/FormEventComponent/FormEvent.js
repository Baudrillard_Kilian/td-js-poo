import app from '../App';
import resources from './FormEventRessources.json';
import template from './FormEvent.ejs';


export class FormEvent {
    domFrom;

    domEventTitle;

    domDateDebut;

    domDateFin;

    domSaveEvent;


    constructor() {
        
        const html = template({ resources: resources });
        const docHtml = app.domParser.parseFromString( html, 'text/html' );
            this.domForm = docHtml.getElementById('event-form');
            this.domEventTitle = this.domForm.querySelector( '#event-title' );
            this.domDateDebut = this.domForm.querySelector('#event-debut');
            this.domDateFin = this.domForm.querySelector('#event-fin');
            this.domSaveEvent = this.domForm.querySelector('#event-save');
            this.domSaveEvent.addEventListener('click', this.eventSave.bind( this ))


    }

    render( domContainer ) {
        domContainer.append( this.domForm );
    }



    eventSave() {
        console.log( "event save" );
    }
}