const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');
const appConfig = require( './app.config.json' );

module.exports = {
    entry: './src/index.js',
    plugins: [
        new webpack.ProvidePlugin({
            _: "underscore"
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Heritage JS Objet',
            template: 'src/index.ejs',
            templateParameters: {
                config: appConfig
            }
        })
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    },
    module: {
        rules: [
            {
                test: /\.ejs$/i,
                exclude: [ path.resolve( __dirname, 'src/index.ejs' ) ],
                use: {
                    loader: 'ejs-loader',
                    options: {
                        variable: 'data'
                    }
                }
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            '@babel/plugin-proposal-object-rest-spread',
                            '@babel/plugin-proposal-private-methods'
                        ]
                    }
                }
            },
            // Traitement des SCSS et CSS
            {
                test: /\.s?css$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            // Traitement des images et des polices
            {
                test: /\.(png|gif|jpg|jpeg|svg|woff2|ttf)$/i,
                use: ['file-loader']
            }
        ]
    }
};